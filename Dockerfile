FROm python:3.7.6-slim

ENV APP_PATH /usr/src/app
WORKDIR $APP_PATH

RUN mkdir -p $APP_PATH \
  && apt-get update && apt-get install -y \
    unixodbc \
    unixodbc-dev \
    libpq-dev \
    python3-dev \
    build-essential \
  && apt-get autoremove -y \
  && rm -rf /var/lib/apt/lists/*

COPY . $APP_PATH

RUN pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt && \
    mkdir -p assets/static && \
    python manage.py collectstatic --noinput